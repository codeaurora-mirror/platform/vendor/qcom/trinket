[early_services/]
mode: 0755
user: AID_ROOT
group: AID_SHELL
caps: 0

[early_services/system/bin/]
mode: 0751
user: AID_ROOT
group: AID_SHELL
caps: 0

[early_services/vendor/bin/]
mode: 0751
user: AID_ROOT
group: AID_SHELL
caps: 0

[early_services/system/bin/*]
mode: 0755
user: AID_ROOT
group: AID_SHELL
caps: 0

[early_services/vendor/bin/*]
mode: 0755
user: AID_ROOT
group: AID_SHELL
caps: 0

[early_services/init_early]
mode: 0750
user: AID_ROOT
group: AID_SHELL
caps: 0

[early_services/init_early_test]
mode: 0750
user: AID_ROOT
group: AID_SHELL
caps: 0
